@ECHO OFF
cls

ECHO. [%time%] - EXECUTE GRADLE BUILD
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

CALL D:\app-dev\configure-env.bat
CD /D %CURRENT_DIR%
cd..

set PROXY_SETTINGS=-Dhttp.proxyHost=127.0.0.1 -Dhttp.proxyPort=3128 -Dhttps.proxyHost=127.0.0.1 -Dhttps.proxyPort=3128 
CALL gradle --stacktrace %PROXY_SETTINGS% publish

PAUSE