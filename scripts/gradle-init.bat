@ECHO OFF
cls

ECHO. [%time%] - EXECUTE GRADLE INIT
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

CALL D:\app-dev\configure-env.bat
CD /D %CURRENT_DIR%
cd..

ECHO %CD%
CALL gradle init

RMDIR /Q /S %CD%/gradle
DEL /Q %CD%/gradlew
DEL /Q %CD%/gradlew.bat

PAUSE